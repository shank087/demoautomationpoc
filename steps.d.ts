/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file.js');
type DbHelper = import('./node_modules/codeceptjs-dbhelper');
type CustomHelpers = import('./helpers/commonHelpers.js');
type CustomEvents = import('./helpers/listeners.js');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any }
  interface Methods extends WebDriver, DbHelper, CustomHelpers, CustomEvents, REST, JSONResponse {}
  interface I extends ReturnType<steps_file>, WithTranslation<DbHelper>, WithTranslation<CustomHelpers>, WithTranslation<CustomEvents>, WithTranslation<JSONResponse> {}
  namespace Translation {
    interface Actions {}
  }
}
