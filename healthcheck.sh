echo "BROWSER NAME IS $BROWSER_NAME"
echo "TAG NAME $TAG_NAME"

while [ "$( curl -s http://hub:4444/wd/hub/status | jq -r .value.ready )" != "true" ]
do
	sleep 1
done


if [ ! -z "$BROWSER_NAME" ]
then
    npx codeceptjs run --grep "$TAG_NAME" --profile="$BROWSER_NAME" --verbose --reporter mocha-multi
else
    npx codeceptjs run-multiple basic --grep "$TAG_NAME" --verbose --reporter mocha-multi 
fi