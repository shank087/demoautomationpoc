///<reference path="./db_helper/db_helper.js"/>
const { event, output, recorder } = require('codeceptjs');
const DBHelper = require('./db_helper/db_helper.js');

require('dotenv').config();

const jiraFailureReport = {
  title: null,
  issueDescription: [],
  tag: null,
  testData: null,
  screenshotName: null,
  jiraID: null,
  failureStepName: null
};

module.exports = function () {
  module.exports = getJiraFailureDetails = () => {
    return jiraFailureReport;
  }

  var jiratestExecID = null;
  const reqDbHelper = require('./db_helper/db_helper.js');
  var api_helper = require('./api_helper/api_helper.js');

  event.dispatcher.on(event.test.passed, async function (test) {
    var dbHelper = new reqDbHelper.DBHelper();
    let testTag = getTags(test);
    let dbResult = await dbHelper.getTestDetails(testTag);
    let jiraIDs = dbResult.filter(e => e.testName.includes(testTag));

    jiraIDs.forEach(async function (jiraDefects, idx) {
      await api_helper().transitIssue(jiraDefects.jiraID);
    });

    await dbHelper.deleteJsonValues(getTags(test));
  })

  event.dispatcher.on(event.test.failed, async function (test, error) {
    jiraFailureReport.issueDescription = [];

    const listeners = require('./listeners.js')
    jiraFailureReport.testData = listeners().testData;
    jiraFailureReport.title = listeners().title;

    jiraFailureReport.tag = getTags(test);

    let message = getFailureError(error)

    jiratestExecID = (global.browserName == 'chrome') ? process.env.CHROME_TEST_ID : process.env.EDGE_TEST_ID;

    var dbHelper = new reqDbHelper.DBHelper();
    let dbErrorMessage = await dbHelper.getTestDetails(jiraFailureReport.tag);
    (dbErrorMessage.length == 0) && await dbHelper.insertJsonValues(jiraFailureReport.tag, '', '');
    let dbResult = await dbHelper.getTestDetails(jiraFailureReport.tag);
    let dbErrorFlag = dbResult.filter(e => e.error.includes(message)).length > 0
    let dbTestFlag = dbResult.filter(e => e.testName == jiraFailureReport.tag).length > 0

    let connectionStatus = await dbHelper.closeConnection();

    if (!dbErrorFlag && (dbTestFlag)) {
      if (!connectionStatus) {
        dbHelper = new reqDbHelper.DBHelper();
      }

      let dbResult = await dbHelper.getTestDetails(jiraFailureReport.tag);
      let dbErrorFlag = dbResult.filter(e => e.error.includes(message)).length > 0
      let dbTestFlag = dbResult.filter(e => e.testName == jiraFailureReport.tag).length > 0

      if (!dbErrorFlag && (dbTestFlag)) {
        await dbHelper.insertJsonValues(jiraFailureReport.tag, message, '');

        generateIssueDescription(test, error);

        await updateJira(test);
        await dbHelper.updateJsonValues(jiraFailureReport.tag, message, jiraFailureReport.jiraID);
        await dbHelper.closeConnection();
      }
    }

  })

  function getTags(test) {
    let tagsArray = test.tags.filter((e) => e.includes('COD'));
    return tagsArray[tagsArray.length - 1];
  }

  function generateIssueDescription(test, error) {
    test.steps.forEach(step => {
      if (!step.name.includes('assert')) {
        jiraFailureReport.issueDescription.push(step.metaStep.actor + ' ' + step.metaStep.name);
      }
      else{
        if (step.name.includes('assert')) jiraFailureReport.failureStepName = step.metaStep.name;
      }
    });
    jiraFailureReport.issueDescription.push('\n Actual Result: ' + error.actual);
    jiraFailureReport.issueDescription.push('\n Expected Result: ' + error.expected);
    jiraFailureReport.issueDescription.push('\n Error Message: ' + error.message);
    if (jiraFailureReport.testData != null) {
      jiraFailureReport.issueDescription.push('\n\n Explicit Test Data Used: ' + jiraFailureReport.testData);
    }
    console.log(jiraFailureReport.issueDescription.join('\r\n'));
  }

  function getFailureError(error) {
    if ('params' in error) {
      const { params } = error;
      return `Error ${params.customMessage} - Expected "${params.jar}" ${params.type} "${params.needle}"`;
    }
    else {
      return `Error ${error.actual}`;
    }
  }

  async function updateJira(test) {
    console.log('Before promise call.')
    let createIssueRes = await api_helper().createIssue();
    jiraFailureReport.jiraID = createIssueRes.key
    await api_helper().createIssueLink();
    await api_helper().attachFormScreenshots(test.artifacts.screenshot);
  };
};
