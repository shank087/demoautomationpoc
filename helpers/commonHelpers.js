/// <reference path="../steps_file.js" />
const Helper = require('@codeceptjs/helper');
const dbjs = require('database-js');

class CommonHelpers extends Helper {
  doAwesomeThings() {
    console.log('Hello from MyHelpr');
  }

  async clickOnEveryElement(locator) {
    const { WebDriver } = this.helpers;
    const els = await WebDriver._locate(locator);

    for (let el of els) {
      await el.click();
    }
  }



  /*   saveScreenshotUsingWebdriver() {
      const { browser } = this.helpers.WebDriver;
       browser.saveScreenshot('E:/codeceptjs/failure_screenshots/screenshot.png');
    }
   */
  async _beforeSuite() {
    let conn = null;
    try {
    //  conn = await new dbjs.Connection('json:///helpers/test.json');
   //   const deleteTable = conn.prepareStatement('DELETE WHERE testName like "%COD%"');
    //  await deleteTable.execute();
    } catch (err) {
      console.error(err);
    } finally {
      if (conn) {
        await conn.close();
      }
    }
  }

  getDBConnection() {
    console.log("hellow")
  }

}

module.exports = CommonHelpers;