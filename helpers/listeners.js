const { execSync } = require('child_process');
const fs = require('fs');
const event = require('codeceptjs').event;
const rimraf = require("rimraf");
const { output } = require('codeceptjs');
require('dotenv').config();
const recorder = require('codeceptjs').recorder;
const path = require('path');
const axios = require('axios').default;

//const {allFeatures} = require('codeceptjs-cucumber-json-reporter/index');

const testScenarioDetails = {
  title: null,
  testData: null,
  unalteredTitle: null,
  jiratestExecID: null,
};
module.exports = function () {
  /**
  * Event dispatcher once the test starts. Using this event to set lavel, feature and description to the allure reports.
  */
  module.exports = getTestScenarioDetails = () => {
    return testScenarioDetails;
  }

  event.dispatcher.on(event.test.started, function (test) {
    const allure = codeceptjs.container.plugins('allure');
    allure.feature(test.parent.title.split("|")[0].trim());
    //allure.setDescription(test.title.split("|")[0].trim());
    allure.setDescription(testScenarioDetails.title + '\n Test Data Used: ' + testScenarioDetails.testData);
  });

  event.dispatcher.on(event.suite.before, function (suite) {
    const allure = codeceptjs.container.plugins('allure');
    browserName = codeceptjs.container.helpers('WebDriver').options.capabilities.browserName;
    suite.title = 'Browser' + ': ' + browserName.toUpperCase() + ' - ' + suite.title;

    if (browserName == 'chrome') {
      suite.tags = suite.tags.filter((e) => e.includes(process.env.CHROME_TEST_ID));
      testScenarioDetails.jiratestExecID = suite.feature.tags.filter((e) => e.name.includes(process.env.CHROME_TEST_ID));

    }
    else {
      suite.tags = suite.tags.filter((e) => e.includes(process.env.EDGE_TEST_ID));
      testScenarioDetails.jiratestExecID = suite.feature.tags.filter((e) => e.name.includes(process.env.EDGE_TEST_ID));
    }
    suite.feature.tags = testScenarioDetails.jiratestExecID
  });

  event.dispatcher.on(event.test.before, function (test) {
    let str = test.title;
    testScenarioDetails.unalteredTitle = test.title;
    if (str.match(/(?<=\{).+?(?=\})/) != null) {
      testScenarioDetails.title = str.match(/^[^\{),]*/)[0]
      testScenarioDetails.testData = str.match(/(?<=\{).+?(?=\})/)[0];
    }
    else {
      testScenarioDetails.title = test.title.split("@")[0].trim();
    }
  });


  event.dispatcher.on(event.all.after, async function () {
    // destination will be created or overwritten by default.
    var api_helper = require('./api_helper/api_helper.js');
    let dir = null;
    let outputDir = null;
    switch (global.browserName.toLowerCase()) {
      case 'chrome':
        dir = "allure-results/basic_chrome_1";
        outputDir = "output/chrome_report";
        break;
      case 'msedge':
        dir = "allure-results/basic_edge_2";
        outputDir = "output/edge_report";
        break;
      default:
        dir = 'allure-results';
        outputDir = "output";
    }

    if(!fs.existsSync(dir)){
      console.log('doesnt exist');
      dir = 'allure-results';
      outputDir = "output";
    }
    const { resolve } = require('path');
    fs.copyFile('helpers/environment.xml', dir + '/environment.xml', (err) => {
      if (err) throw err;
      console.log('File was copied to destination');
    });

    const fse = require('fs-extra');

    let dirPath = path.join(__dirname, '../' + dir + '/');
    fs.readdirSync(dirPath).forEach(file => {
      if (file == 'assets' || file.includes('report') || file.includes('result.xml')) { //
        fse.moveSync(path.resolve(__dirname, `../${dir}/${file}`), path.resolve(__dirname, `../${outputDir}/${file}`), { overwrite: true | false });
      }
    });


    var zipper = require('zip-local');
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds();
    var zipFileName = global.browserName +'_TestReports'+date + time;
    outputDir = path.resolve(__dirname, `../${outputDir}/`);
    zipper.sync.zip(outputDir).compress().save(`${outputDir}/${zipFileName}.zip`);

    
    if(!process.env.BROWSER_NAME){
      await api_helper().attachFormScreenshots(`${outputDir}/${zipFileName}.zip`, testScenarioDetails.jiratestExecID[0].name.replace('@', ''));
    }
    await api_helper().attachFormScreenshots(`${outputDir}/${zipFileName}.zip`, process.env.TAG_NAME);

    const ALLURE_RESULTS_DIRECTORY = 'allure-results/'

    const ALLURE_SERVER = 'https://882a-43-225-165-61.in.ngrok.io'

    var fileData = []

    fs.readdirSync(path.join(__dirname, '../' + dir)).forEach(file => {
      if (file != '1' && file !='default') {
        let data = path.join(__dirname, '../' + dir + '/' + file);
        fileData.push(data);
        // fileData.push('files[]='+data);
      }
    });

    console.log('Files for allure report' + fileData);

    allureReportURL = await generateAllureReports(fileData, ALLURE_SERVER);
    
    console.log(allureReportURL);
    if(!process.env.BROWSER_NAME){
      console.log("Updating Test EXECUTION DETAILS");
      await api_helper().addIssueComments(testScenarioDetails.jiratestExecID[0].name.replace('@', ''), allureReportURL);
    }
    await api_helper().addIssueComments(process.env.TAG_NAME, allureReportURL);
    console.log(process.env.TAG_NAME);
    await api_helper().transitIssue(process.env.TAG_NAME);

  });
0
  async function generateAllureReports(files, server){
    console.log(global.browserName.toLowerCase())
      if(global.browserName.toLowerCase()!='chrome'){
        await new Promise(resolve => setTimeout(resolve, 10000));
      }  
      await publishAllureReports(files, server);
      let allureReportURL = await getAllureReport(server)
      return allureReportURL;
  }

  async function publishAllureReports(files, server) {
    const FormData = require('form-data');
    const form = new FormData();
    files.forEach(function (file, idx) {
        form.append('files[]', fs.createReadStream(file));
    });

    console.log("forms for allure are "+ form);

    await axios.post(`${server}/allure-docker-service/send-results?project_id=1`, form, {
      headers: {
        "UploadCommand": form,
        'Content-Type': `multipart/form-data; boundary=${form._boundary}`,
        ...form.getHeaders()
      }
    }).then(function (response) {
      console.log('Sent');
    }).catch(function (error) {
      console.log('Error on Sending');
    })
  }

  async function getAllureReport(server) {
    const resp = await axios.get(`${server}/allure-docker-service/generate-report?project_id=1&execution_name=TEST_EXECUTION&execution_from=codecept&execution_type=colud`, {
      headers: {
      'Content-Type': 'application/json'
      }
    })
    console.log("reponse data from allure is "+resp.data.data.report_url);
    return resp.data.data.report_url;
  }

}  