const JiraApi = require('jira').JiraApi;
const FormData = require('form-data');
const fs = require('fs');
const axios = require('axios').default;
const btoa = require('btoa');
const jiraApiSchemas = require('./jira_json_schema/jira_api_schemas.js');
const events = require('../events.js')

module.exports = function () {
    var jira = new JiraApi('https', 'sriharijuly.atlassian.net/', '', 'sriharijuly1987@gmail.com', encodeJIRAPassword(), '2');

    function createIssue() {
        console.log("hello");
        jiraApiSchemas.createIssueBody.fields.summary = constructDefectSummary();
        jiraApiSchemas.createIssueBody.fields.description = events().issueDescription.join('\r\n');
        jiraApiSchemas.createIssueBody.fields.environment = "Test Env: QC AND Test Browser: " + global.browserName;

        return new Promise((resolve, reject) => {
            jira.addNewIssue(jiraApiSchemas.createIssueBody, function (error, response) {
                resolve(response)
            })
        })
    }

    function createIssueLink() {
        jiraApiSchemas.linkIssueBody.inwardIssue.key = events().jiraID;
        jiraApiSchemas.linkIssueBody.outwardIssue.key = events().tag.replace('@', '');

        return new Promise((resolve, reject) => {
            jira.issueLink(jiraApiSchemas.linkIssueBody, function (error, response) {
                resolve(response)
            })
        })
    }

    function transitIssue(jiraID) {
        return new Promise((resolve, reject) => {
            jira.transitionIssue(jiraID, jiraApiSchemas.transitIssue, function (error, response) {
                resolve(response)
            })
        })
    }

    function addIssueComments(jira_id, bodyData) {
        return new Promise((resolve, reject) => {
            jira.addComment(jira_id, 'Please view the allure report at :'+ bodyData, function (error, response) {
                resolve(response)
            })
        })
    }

    async function addIssueComments1(jira_id, bodyData) {
        var username = 'sriharijuly1987@gmail.com';
        var password = encodeJIRAPassword();
        var basicAuth = 'Basic ' + btoa(username + ':' + password);

        await axios.post(`https://sriharijuly.atlassian.net/rest/api/2/issue/${jira_id}/comment`, {
            headers: {
                'Authorization': basicAuth,
                'Content-Type': 'application/json',
                'X-Atlassian-Token': 'nocheck',
            },
            data: {
                'body': 'Please view the allure report at :'+ bodyData
            }
        }).then(function (response) {
            console.log('Added the comments');
        }).catch(function (error) {
            console.log('Error in adding comments');
        })
    }

    async function attachFormScreenshots(filepath, jira_id) {
        if (jira_id == undefined) {
            jira_id = events().jiraID;
        }

        const form = new FormData();
        form.append('file', fs.createReadStream(filepath));

        var username = 'sriharijuly1987@gmail.com';
        var password = encodeJIRAPassword();
        var basicAuth = 'Basic ' + btoa(username + ':' + password);

        await axios.post(`https://sriharijuly.atlassian.net//rest/api/2/issue/${jira_id}/attachments`, form, {
            headers: {
                'Authorization': basicAuth,
                "UploadCommand": form,
                'Content-Type': `multipart/form-data; boundary=${form._boundary}`,
                'X-Atlassian-Token': 'nocheck',
                ...form.getHeaders()
            }
        }).then(function (response) {
            console.log('Authenticated');
        }).catch(function (error) {
            console.log('Error on Authentication');
        })
    }

    function encodeJIRAPassword() {
        const CryptoJS = require('crypto-js')

        var key = "ASECRET";
        jiraKey = 'U2FsdGVkX1+7/WyLdwTMcyyXSCW/bqIkIQrZpIyqnxbJ9dSNeeNbxpeWWuKo1D7O';

        var decipher = CryptoJS.AES.decrypt(jiraKey, key);
        decipher = decipher.toString(CryptoJS.enc.Utf8);
        return decipher
    }

    function constructDefectSummary() {
        return "Defect: TC_ID: " + events().tag + ": " + "Failure while asserting " + `"${events().failureStepName}"`; //+ events().title.replace('Validate ', '');
    }

    return {
        createIssue: createIssue,
        createIssueLink: createIssueLink,
        attachFormScreenshots: attachFormScreenshots,
        transitIssue: transitIssue,
        addIssueComments: addIssueComments
    }
}
