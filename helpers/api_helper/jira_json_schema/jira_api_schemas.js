let jiraSchemas = {
    createIssueBody: {
        "fields": {
            "project":
            {
                "key": "COD"
            },
            "summary": '',
            "description": '',
            "issuetype": {
                "name": "Bug"
            },
            "environment": "Test Env: QC AND Test Browser: " + '',
        }
    },
    linkIssueBody: {
        "outwardIssue": {
            "key": '',
        },
        "comment": {
            "body": "Linked related issue!",
        },
        "inwardIssue": {
            "key": '',
        },
        "type": {
            "name": "Blocks"
        }
    },

    transitIssue: {
        "update": {
            "comment": [
                {
                    "add": {
                        "body": "Bug has been fixed."
                    }
                }
            ]
        },
        "transition": {
            "id": "31"
        }
    }
}

module.exports = jiraSchemas;