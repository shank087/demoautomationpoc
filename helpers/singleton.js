class SingletonClass {

  constructor() {
    if (SingletonClass._instance) {
      return SingletonClass._instance
    }
    SingletonClass._instance = this;
    this.map = new Map();
    this.testTags = [];
  }
  getMap() {
    if(this.map.size == 0){
      return this.map;
    }
    
  }

  gettestTags() {
    if(this.testTags.length == 0){
      return this.testTags;
    }
  }
}

module.exports = new SingletonClass();