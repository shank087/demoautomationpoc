class DBHelper{
    conn = null;
    constructor(){
        const dbjs = require('database-js');
        this.conn =  new dbjs.Connection('json:///helpers/test.json');
    }

    async getTestDetails(testTag) {
        let getDBErrorMessage = this.conn.prepareStatement(`SELECT testName, error, jiraID WHERE testName='${testTag}'`);
        let res =  await getDBErrorMessage.query();
        return res;
    }

    async insertJsonValues(testTag, errorMsg, defectID){
        const updateJSON = this.conn.prepareStatement("INSERT VALUES {'testName':'" + testTag + "', 'error':'" + errorMsg + "', 'jiraID':'" + defectID + "'}");
        await updateJSON.execute();
    }

    async updateJsonValues(testTag, errorMsg, defectID){
        const updateJSON = this.conn.prepareStatement( `UPDATE SET jiraID = '${defectID}' WHERE testName = '${testTag}' AND  error = '${errorMsg}' ` );
        await updateJSON.execute();
    }

    async deleteJsonValues(testTag){
        const deleteTable = this.conn.prepareStatement(`DELETE WHERE testName = '${testTag}'`);
        await deleteTable.execute();
    }   
    closeConnection(){
        if(this.conn) {
            this.conn.close();
            return true;
        }
        return false;
    }

}

module.exports.DBHelper = DBHelper;