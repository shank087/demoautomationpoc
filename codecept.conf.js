const dotenv = require('dotenv').config();


exports.config = {
  output: './allure-results/',
  helpers: {
    WebDriver: {
      url: 'https://codecept.io/',
      host: 'hub',
      browser: process.profile,
      windowSize: 'maximize',
      smartWait: 8000,
    },
    DbHelper: {
      require: "./node_modules/codeceptjs-dbhelper"
    },
    CustomHelpers: {
      require: './helpers/events.js',
    },
    CommonHelpers: {
      require: './helpers/commonHelpers.js',
    },
    ChaiWrapper: {
      require: "codeceptjs-chai"
    },
    CustomEvents: {
      require: './helpers/listeners.js'
    },
    REST: {
      prettyPrintJson: true,
    },
    JSONResponse: {
      requestHelper: 'REST',
    }

  },
  include: {
    I: './steps_file.js'

  },
  async bootstrap() {
    share({ map: false });
    share({ tags: false });
  },
  mocha: {
    "reporterOptions": {
      "mochawesome": {
        "uniqueScreenshotNames": "true",
        "stdout": "./output/console.log",
        "options": {
          "reportDir": "./allure-results/",
          "reportFilename": "report",
          "attachments": true
        }
      },
      "mocha-junit-reporter": {
        "uniqueScreenshotNames": "true",
        "stdout": "./output/console.log",
        "options": {
          "mochaFile": "./allure-results/result.xml",
          "attachments": true //add screenshot for a failed test
        }
      }
    }
  },
  timeout: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './tests/features/*.feature',
    steps: ['./tests/step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: true,
      //uniqueScreenshotNames: true
    },
    wdio: {
      enabled: false,
      services: ['selenium-standalone']
    },
    allure: {
      // outputDir: './allure-results/',
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    retryFailedStep: {
      enabled: false
    },
    retryTo: {
      enabled: true
    },
    eachElement: {
      enabled: true
    },
    pauseOnFail: {},
    cucumberJsonReporter: {
      require: 'codeceptjs-cucumber-json-reporter',
      enabled: true,               // if false, pass --plugins cucumberJsonReporter
      attachScreenshots: true,     // true by default
      attachComments: true,        // true by default
      outputFile: 'file.json',     // cucumber_output.json by default
      uniqueFileNames: false,      // if true outputFile is ignored in favor of unique file names in the format of `cucumber_output_<UUID>.json`.  Useful for parallel test execution
      includeExampleValues: true,  // if true incorporate actual values from Examples table along with variable placeholder when writing steps to the report
      timeMultiplier: 1000000,     // Used when calculating duration of individual BDD steps.  Defaults to nanoseconds
    },
  },
  stepTimeout: 0,
  stepTimeoutOverride: [{
    pattern: 'wait.*',
    timeout: 0
  },
  {
    pattern: 'amOnPage',
    timeout: 0
  }
  ],
  // tests: './webdriver_tests/*_test.js',
  multiple: {
    "basic": {
      // run all tests in chrome and firefox
      "browsers": [
        {
          browser: "chrome",
          outputName: "chrome",
          //   outputDir: './allure-results/*chrome*/'
        },
        {
          browser: "MicrosoftEdge",
          outputName: "edge",
          //  outputDir: './allure-results/*egde*/'
        }
      ]
    },
  },
  name: 'tests',
  jira: {
    username: 'sriharijuly1987@gmail.com',
    password: 'U2FsdGVkX1+7/WyLdwTMcyyXSCW/bqIkIQrZpIyqnxbJ9dSNeeNbxpeWWuKo1D7O',
    baseURL: 'sriharijuly.atlassian.net/'
  }
}