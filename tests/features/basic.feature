@COD-160 @COD-164 @COD-2
Feature: Test Codeceptjs integration with JIRA and CI Recipes
  POC to check CodeceptJS integration with JIRA and CI Recipes

  @COD-3 @smoke
  Scenario: Validate the title in CodeceptJS Homepage
    Given I navigate to URL
    Then I see title "JSCodecept"

  @COD-4 @smoke
  Scenario Outline: Validate the title in Releases page
    Given I navigate to URL
    When I click on "Releases"
    Then I see text "<Expected Label>"
    And I see header "<Expected Page Header>"

    Examples:
      | Url                 | Expected Label | Expected Page Header |
      | https://codecept.io | Playwright     | Releases             |

  @COD-154 @smoke
  Scenario: Validate the title in CodeceptJS Homepage
    Given I navigate to URL
    Then I see title "CodeceptJS"
