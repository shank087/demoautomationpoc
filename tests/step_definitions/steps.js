const { I } = inject();


// Add in your custom step files

Given('I navigate to URL', () => {
  I.amOnPage('/');
});

Then('I see title {string}', async (title) => {
  let locator = { xpath: '//span[text()="CodeceptJS"]' };
  let browserText = await I.grabTextFrom(locator);
  I.assertEqual(browserText, title, 'Both the values are not equal');
});


When('I click on {string}', (link) =>{
  I.click(link);
});


Then('I see text {string}', async (expectedText) => {
  browserName = codeceptjs.container.helpers('WebDriver').browser.capabilities.browserName.toUpperCase();
 
  let browserText = await I.grabTextFrom({ xpath: '//li[text()[normalize-space()="Playwright"]]' });
  if(browserName == "CHROME"){
    I.assertEqual(browserText.trim(), 'Puppeteer', 'Both the values are not equal');
  }
  else{
    I.assertContain(browserText.trim(), expectedText, 'Both the values are equal');
  }
  
});

Then('I see header {string}', async (header) => {
  browserName = codeceptjs.container.helpers('WebDriver').browser.capabilities.browserName.toUpperCase();

  let appHeader = await I.grabTextFrom({id:'releases'});
  if(browserName == "CHROME"){
    I.assertEqual(appHeader.trim(), header, 'Both the values are equal');
  }
  else{
    I.assertEqual(appHeader, 'Quickstartttttttttt', 'Both the values are not equal');
  }
  
});