FROM codeceptjs/codeceptjs
RUN apt-get install -y openjdk-8-jdk
RUN apt install -y jq
WORKDIR /app
COPY . .
ADD healthcheck.sh healthcheck.sh
RUN npm install
ENTRYPOINT sh healthcheck.sh